import logo from './logo.svg';
import './App.css';
import React from "react";
import Creer from './components/Creer';
import Personne from './components/Personne';
import Modifier from './components/Modifier';

function App() {

  

  return (
    <div className="flex-container">
      <Creer />
      <Personne/>
      
    </div>
  );
}

export default App;
