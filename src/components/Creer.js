import React, { useState } from 'react'
import firebase from'../util/firebase'

const Creer = () => {
    const[name,setName]= useState('')
    const[pause,setPause]= useState('15 min')
    const[time,setTime]= useState(Date.now())

    //new Date(Date.now()).toUTCString()

    const Ajouter = () => {
        const db =firebase.database().ref('Utilisateurs')
        const utilisateur = {
            name,
            pause,
            time,
    }
        db.push(utilisateur)
    setName('')
    setPause('900')
    setTime(Date.now())
    //new Date(Date.now()).toUTCString()
}


    return (
        <div className="form-control">
            <h4>Entrer votre nom svp :</h4>
            <input 
            type='text'
            placeholder='name '
            value={name}
            onChange={(e)=> setName(e.target.value)}
            />
            <button onClick={Ajouter} > Ajouter </button>
            </div>
    )
}

export default Creer
