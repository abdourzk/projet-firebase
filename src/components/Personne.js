import React, { useState, useEffect } from "react";
import firebase from "../util/firebase";
import Modifier from "./Modifier";

const Personne = () => {
  const [Liste, setListe] = useState([]);
  const [Comps,setComps] =useState([])


  useEffect(() => {
    const db = firebase.database().ref("Utilisateurs");

    db.on("value", (snapshot) => {
      let Utilisateurs = snapshot.val();
      let A = [];
      for (let id in Utilisateurs) {
        A.push({ id, ...Utilisateurs[id] });
      }
      console.log(Liste);
      setListe(A);
    });
  }, []);



  return (
    <div className="ListePersonne">
                  <ol>
              
      {Liste &&
        Liste.map((personne) => {
          return (

                <div>
                              <Modifier Utilisateur={personne}/>
                </div>
              

          );
        })}
                    </ol>
    </div>
  );
};

export default Personne;
