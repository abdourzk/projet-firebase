import firebase from 'firebase'

var firebaseConfig = {
  apiKey: "AIzaSyBJPE6OwsQMnq0QMHelNPgYO_eob3RkQd8",
  authDomain: "test-28c10.firebaseapp.com",
  databaseURL: "https://test-28c10-default-rtdb.firebaseio.com",
  projectId: "test-28c10",
  storageBucket: "test-28c10.appspot.com",
  messagingSenderId: "911932149612",
  appId: "1:911932149612:web:ea0bcf571a1f69c8d72d49"
};// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();


export default firebase;
